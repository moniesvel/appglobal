import 'package:app_maintain_my_vehicule/src/pages/home_page.dart';
import 'package:app_maintain_my_vehicule/src/providers/provider.dart';
import 'package:flutter/material.dart';

import 'src/pages/auth/login_page.dart';


 
void main() => runApp(MyApp());
 
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    
    return Provider(
      
      child: MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'JYMY APP',
      initialRoute: 'Home',
      routes: {
        'login' : (BuildContext context) => LoginPage(),
        'Home'  : (BuildContext context) => HomePage()
      },
    )
    ); 
  }
}