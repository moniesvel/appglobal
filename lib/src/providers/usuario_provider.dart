
import 'dart:convert';

import 'package:app_maintain_my_vehicule/src/models/usuario_model.dart';
import 'package:http/http.dart' as http;

class UsuarioProvider {

    final String _url= 'https://apisjymy.000webhostapp.com/api';

   Future<Map<String, dynamic>>crearUsuario ( UsuarioModel usuario) async {

      final url = '$_url/createUsuarios.php';

      final resp = await http.post(url, body:usuarioModelToJson(usuario));
      
      
        if(resp.statusCode == 200){
        

          return {'ok': true, 'mensaje':resp.body}; 

        }else{
          
          return {'ok':false, 'mensaje':resp.body}; 
        
        }
    

    }
  Future<Map<String, dynamic>> validarUsuario( String email, String clave) async{

       final url = '$_url/getUsuarios.php?';

       final resp = await http.post('$url email=$email&clave=$clave');
       print('body:'+resp.body);

        if(resp.statusCode == 200){
          
          final decodeData = json.decode(resp.body);

          return {'ok': true, 'data':decodeData}; 

        }else{
          
          return {'ok':false, 'mensaje':resp.body}; 
        
        }
    
  }



}