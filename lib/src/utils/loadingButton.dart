import 'package:flutter/material.dart';


class LoadingButton extends StatelessWidget {

  
      bool isloading;
      Widget child;
      Function onPressed;
      Color backgroundColor;
      Color loaderColor;
      Color textColor;
      ShapeBorder shapeBorder;

    LoadingButton({
      @required this.isloading,
      @required this.child,
      @required this.onPressed,
      this.backgroundColor,
      this.loaderColor,
      this.textColor,
      this.shapeBorder

    });

  @override
  Widget build(BuildContext context) {

    return (isloading) ? CircularProgressIndicator(
        backgroundColor: Colors.grey.shade200,
        valueColor: AlwaysStoppedAnimation(this.loaderColor)
        )
      :RaisedButton(     
      child:child,
      onPressed: onPressed,
      color: backgroundColor,
      textColor: textColor,
      shape: shapeBorder,

    );
  }
}