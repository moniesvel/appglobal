


import 'dart:async';

import 'package:flutter/material.dart';

class Validators {

final validarEmail =  StreamTransformer<String, String>.fromHandlers(
    handleData: ( email, sink)
    {

      Pattern pattern = r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
      RegExp regExp = new RegExp(pattern);

      if(regExp.hasMatch(email)){
        sink.add(email);
      }else{  
        sink.addError('Email no Válido!');
      }
    
    }
  );


  final validarClave =  StreamTransformer<String, String>.fromHandlers(
    handleData: ( clave, sink)
    {

      if( clave.length >= 6){
        sink.add( clave );
      } else {
        sink.addError('Más de 6 Caracteres por favor!');
      }
    
    }
  );



    

}
  bool isNumeric( String s ){
      if(s.isEmpty) return false;

      final n = num.tryParse(s);

      return (n == null) ? false : true;
  }
  bool isEmail(String s ){

      if(s.isEmpty) return false;

      Pattern pattern = r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
      RegExp regExp = new RegExp(pattern);

      if(regExp.hasMatch(s)){
         return true;
      }else{  
        return false;
      }

  }
  void mostrarAlerta(BuildContext context, String resp){

      showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text('Información'),
            content: Text(resp),
            actions: <Widget>[
              FlatButton(
                child: Text('Ok'),
                onPressed: ()=> Navigator.of(context).pop(),
                )
            ],
          );
        }  
      );
  }