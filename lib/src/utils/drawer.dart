import 'package:app_maintain_my_vehicule/src/pages/auth/login_page.dart';
import 'package:app_maintain_my_vehicule/src/pages/home_page.dart';
import 'package:app_maintain_my_vehicule/src/pages/mantenimiento_page.dart';
import 'package:app_maintain_my_vehicule/src/pages/manual/manual_page.dart';
import 'package:app_maintain_my_vehicule/src/pages/mapa_page.dart';
import 'package:app_maintain_my_vehicule/src/pages/vehiculos_page.dart';
import 'package:app_maintain_my_vehicule/src/utils/BouncyPageRoute.dart';
import 'package:flutter/material.dart';
class AppDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Drawer(      
      child: ListView(
        padding: EdgeInsets.zero,
        children: <Widget>[
          _createHeader(),
          _createDrawerItem(
            icon: Icons.home,
            text: 'Inicio',
            
            onTap: ()=> Navigator.pushReplacement( context, BouncyPageRoute(widget: HomePage()))
                      
            ),
          _createDrawerItem(
            icon: Icons.drive_eta,
            text: 'Mis Vehiculos',
            onTap: ()=> Navigator.pushReplacement( context, BouncyPageRoute(widget: VehiculosPage()))
                      
            ),
          _createDrawerItem(
           icon: Icons.event,
           text: 'Mantenimiento de Vehiculo',
           onTap: ()=> Navigator.pushReplacement( context, BouncyPageRoute(widget: MantenimientoPage()))
           ),
          _createDrawerItem(icon: Icons.location_on, 
          text: 'Mapa de Ubicación',
          onTap: ()=> Navigator.pushReplacement(context, BouncyPageRoute(widget: MapaPage()))
          ),
          _createDrawerItem(icon: Icons.library_books, 
          text: 'Manual',
          onTap: ()=> Navigator.pushReplacement(context, BouncyPageRoute(widget: ManualPage()))
          ),
          Divider(),
          _createDrawerItem(icon: Icons.exit_to_app, 
          text: 'Cerrar Sesión', 
          onTap: ()=> Navigator.pushReplacement(context, BouncyPageRoute(widget: LoginPage()))),
        ],
      ),
    );
  }
  Widget _createHeader() {
  return DrawerHeader(
      decoration: BoxDecoration(
      gradient: LinearGradient(colors: <Color>[
        Colors.deepOrange,
        Colors.orangeAccent
      ])
      ),
      child: Container(
        child: Column(
           children: <Widget>[
             Material(
               
               borderRadius: BorderRadius.all(Radius.circular(10.0)),
               child: Padding(padding: EdgeInsets.all(5.0),
               child: Image.asset('assets/img/logo.jpeg', width: 100, height: 100,)
               ),
             ),
           ],
        ),
      ),
  );
          
}
Widget _createDrawerItem(
    {IconData icon, String text, GestureTapCallback onTap}) {
  return ListTile(
    title: Row(
      children: <Widget>[
        Icon(icon),
        Padding(
          padding: EdgeInsets.only(left: 8.0),
          child: Text(text,style: TextStyle(fontSize: 17),),
        )
      ],
    ),
    onTap: onTap,
  );
}
}