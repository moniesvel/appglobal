// To parse this JSON data, do
//
//     final mantenimientoModel = mantenimientoModelFromJson(jsonString);

import 'dart:convert';

MantenimientoModel mantenimientoModelFromJson(String str) => MantenimientoModel.fromJson(json.decode(str));

String mantenimientoModelToJson(MantenimientoModel data) => json.encode(data.toJson());

class MantenimientoModel {
    MantenimientoModel({
        this.id,
        this.tipo,
        this.marca,
        this.placa,
        this.usuarioId,
    });

    String id;
    String tipo;
    String marca;
    String placa;
    int usuarioId;

    factory MantenimientoModel.fromJson(Map<String, dynamic> json) => MantenimientoModel(
        id: json["id"],
        tipo: json["tipo"],
        marca: json["marca"],
        placa: json["placa"],
        usuarioId: json["usuario_id"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "tipo": tipo,
        "marca": marca,
        "placa": placa,
        "usuario_id": usuarioId,
    };
}
