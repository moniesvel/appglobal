// To parse this JSON data, do
//
//     final mantenimientoModel = mantenimientoModelFromJson(jsonString);

import 'dart:convert';

UsuarioModel usuarioModelFromJson(String str) => UsuarioModel.fromJson(json.decode(str));

String usuarioModelToJson(UsuarioModel data) => json.encode(data.toJson());

class UsuarioModel {
    UsuarioModel({
        this.id,
        this.nombre,
        this.apellido1,
        this.apellido2,
        this.telefono,
        this.email,
        this.clave,
    });

    String id;
    String nombre;
    String apellido1;
    String apellido2;
    int telefono;
    String email;
    String clave;

    factory UsuarioModel.fromJson(Map<String, dynamic> json) => UsuarioModel(
        id: json["id"],
        nombre: json["nombre"],
        apellido1: json["apellido1"],
        apellido2: json["apellido2"],
        telefono: json["telefono"],
        email: json["email"],
        clave: json["clave"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "nombre": nombre,
        "apellido1": apellido1,
        "apellido2": apellido2,
        "telefono": telefono,
        "email": email,
        "clave": clave,
    };
}
