

// To parse this JSON data, do
//
//     final mantenimientoModel = mantenimientoModelFromJson(jsonString);

import 'dart:convert';

MantenimientoModel mantenimientoModelFromJson(String str) => MantenimientoModel.fromJson(json.decode(str));

String mantenimientoModelToJson(MantenimientoModel data) => json.encode(data.toJson());

class MantenimientoModel {
    MantenimientoModel({
        this.id,
        this.descripcion,
        this.fecha,
        this.vehiculoId,
    });

    String id;
    String descripcion;
    DateTime fecha;
    int vehiculoId;

    factory MantenimientoModel.fromJson(Map<String, dynamic> json) => MantenimientoModel(
        id: json["id"],
        descripcion: json["descripcion"],
        fecha: json["fecha"],
        vehiculoId: json["vehiculo_id"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "descripcion": descripcion,
        "fecha": fecha,
        "vehiculo_id": vehiculoId,
    };
}
