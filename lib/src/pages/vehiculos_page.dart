import 'package:app_maintain_my_vehicule/src/utils/drawer.dart';
import 'package:flutter/material.dart';

class VehiculosPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Mis Vehiculos'),
      ),
      drawer: AppDrawer(),
    );
  }
}