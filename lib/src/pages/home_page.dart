import 'package:app_maintain_my_vehicule/src/models/usuario_model.dart';
import 'package:app_maintain_my_vehicule/src/utils/drawer.dart';
import 'package:flutter/material.dart';

class HomePage extends StatelessWidget {
  
  UsuarioModel usuarioModel = new UsuarioModel();
  @override
  
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Home'),
      ),
      drawer: AppDrawer(),
    );
  }
}