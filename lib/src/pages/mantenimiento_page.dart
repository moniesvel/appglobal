import 'package:app_maintain_my_vehicule/src/utils/drawer.dart';
import 'package:flutter/material.dart';


class MantenimientoPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Mantenimiento de Vehiculo'),
      ),
      drawer: AppDrawer(),
    );
  }
}