import 'dart:async';

import 'package:app_maintain_my_vehicule/src/pages/auth/register_page.dart';
import 'package:app_maintain_my_vehicule/src/pages/home_page.dart';
import 'package:app_maintain_my_vehicule/src/providers/provider.dart';
import 'package:app_maintain_my_vehicule/src/providers/usuario_provider.dart';
import 'package:app_maintain_my_vehicule/src/utils/BouncyPageRoute.dart';
import 'package:app_maintain_my_vehicule/src/utils/loadingButton.dart';
import 'package:app_maintain_my_vehicule/src/utils/validators.dart' as utils;
import 'package:flutter/material.dart';

class LoginPage extends StatefulWidget {

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
     final usuarioProvider = new UsuarioProvider();
  bool isloading = false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Stack(
      children: <Widget>[_crearFondo(context), _loginForm(context)],
    ));
  }

  Widget _crearFondo(BuildContext context) {
    final size = MediaQuery.of(context).size;
    final fondoMorado = Container(
      height: size.height * 0.4,
      width: double.infinity,
      decoration: BoxDecoration(
          gradient: LinearGradient(colors: <Color>[
        Color.fromRGBO(46, 134, 193, 1.0),
        Color.fromRGBO(46, 115, 193, 1.0)
      ])),
    );

    return Stack(
      children: <Widget>[
        fondoMorado,
        Container(
          padding: EdgeInsets.only(top: 60.0),
          child: Column(
            children: <Widget>[
              Icon(
                Icons.person_pin,
                color: Colors.white,
                size: 100.0,
              ),
              SizedBox(
                height: 10.0,
                width: double.infinity,
              ),
              Text('JYMY',
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 30.0,
                  ))
            ],
          ),
        ),
      ],
    );
  }

  Widget _loginForm(BuildContext context) {
    final bloc = Provider.of(context);

    final size = MediaQuery.of(context).size;
    return SingleChildScrollView(
      child: Column(
        children: <Widget>[
          SafeArea(
              child: Container(
            height: 190.0,
          )),
          Container(
            margin: EdgeInsets.symmetric(vertical: 30),
            padding: EdgeInsets.symmetric(vertical: 50.0),
            width: size.width * 0.85,
            decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(5.0),
                boxShadow: <BoxShadow>[
                  BoxShadow(
                      color: Colors.black26,
                      blurRadius: 3.0,
                      offset: Offset(0.0, 5.0),
                      spreadRadius: 3.0)
                ]),
            child: Column(
              children: <Widget>[
                Text(
                  'Inicio de Sesión',
                  style: TextStyle(fontSize: 20.0),
                ),
                SizedBox(
                  height: 30.0,
                ),
                _crearEmail(bloc),
                SizedBox(
                  height: 20.0,
                ),
                _crearPassword(bloc),
                SizedBox(
                  height: 20.0,
                ),
                _crearBoton(bloc),
                FlatButton(
                    child: Text("Desea registrarse?"),
                    onPressed: ()=> Navigator.push(context, BouncyPageRoute(widget:RegisterPage())),
                ),
              ],
            ),
          ),
          
          SizedBox(
            height: 30.0,
          )
        ],
      ),
    );
  }

  Widget _crearEmail(LoginBloc bloc) {
    return StreamBuilder(
      stream: bloc.emailStream,
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        return Container(
          padding: EdgeInsets.symmetric(horizontal: 20.0),
          child: TextField(
            keyboardType: TextInputType.emailAddress,
            decoration: InputDecoration(
                icon: Icon(Icons.alternate_email, color: Colors.blue[800]),
                labelText: 'Correo electrónico',
                errorText: snapshot.error),
            onChanged: bloc.changeEmail,
          ),
        );
      },
    );
  }

  Widget _crearPassword(LoginBloc bloc) {
    return StreamBuilder(
      stream: bloc.claveStream,
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        return Container(
          padding: EdgeInsets.symmetric(horizontal: 20.0),
          child: TextField(
            obscureText: true,
            decoration: InputDecoration(
                icon: Icon(Icons.lock_outline, color: Colors.blue[800]),
                labelText: 'Contraseña',
                errorText: snapshot.error),
            onChanged: bloc.changeClave,
          ),
        );
      },
    );
  }

  Widget _crearBoton(LoginBloc bloc) {

    return StreamBuilder(
      
      stream: bloc.formValidStream,
      
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        return LoadingButton(

          onPressed: snapshot.hasData?(){  

                  setState(() {
                    isloading = true; 
                    print(isloading);
                    _login(bloc, context);
                  });
                  Timer(Duration(seconds: 10),(){
                    setState(() {
                      isloading = false;
                    });
                  });                
              
          } : null,
         isloading: isloading,
          child: Container(
              padding: EdgeInsets.symmetric(horizontal: 80.0, vertical: 15.0),
              child: Text('Ingresar'),   
          ),
          backgroundColor: Colors.deepOrange,
          shapeBorder: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(5.0)),
          textColor: Colors.white,
          loaderColor: Colors.blue[800],      
        );
      },
    );
  }

  _login(LoginBloc bloc, BuildContext context) async {

    print('===================');
    print('Email:${bloc.email}');
    print('clave:${bloc.clave}');
    print('===================');

    Map resp = await usuarioProvider.validarUsuario(bloc.email, bloc.clave);

    if(resp['ok']){
      Navigator.push(context, BouncyPageRoute(widget: HomePage()));
    } else{
       
       utils.mostrarAlerta(context,resp['mensaje']);
       setState(() {
         isloading = false;
       });
    }


  }
}
