
import 'dart:async';

import 'package:app_maintain_my_vehicule/src/models/usuario_model.dart';
import 'package:app_maintain_my_vehicule/src/providers/usuario_provider.dart';
import 'package:app_maintain_my_vehicule/src/utils/BouncyPageRoute.dart';
import 'package:app_maintain_my_vehicule/src/utils/validators.dart' as utils;


import 'package:flutter/material.dart';

import 'login_page.dart';

class RegisterPage extends StatefulWidget {
  @override
  _RegisterPageState createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  final formKey = GlobalKey<FormState>();
  final usuarioProvider = new UsuarioProvider();
  UsuarioModel usuario = new UsuarioModel();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: <Widget>[
          _crearFondo(context),
          _registerForm(context),
        ],
      ),
    );
  }

  Widget _registerForm(BuildContext context) {
    final size = MediaQuery.of(context).size;

    return SingleChildScrollView(
      child: Column(
        children: <Widget>[
          SafeArea(
              child: Container(
            height: 60.0,
          )),
          Container(
            margin: EdgeInsets.symmetric(vertical: 15.0),
            padding: EdgeInsets.symmetric(vertical: 15.0),
            width: size.width * 0.85,
            decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(5.0),
                boxShadow: <BoxShadow>[
                  BoxShadow(
                      color: Colors.black26,
                      blurRadius: 3.0,
                      offset: Offset(0.0, 5.0),
                      spreadRadius: 3.0)
                ]),
            child: SingleChildScrollView(
              child: Container(
                padding: EdgeInsets.symmetric(horizontal: 15.0, vertical: 10.0),
                child: Form(
                  key: formKey,
                  child: Column(
                    children: <Widget>[
                      Text(
                        'Registro de Usuario',
                        style: TextStyle(fontSize: 20.0),
                      ),
                      SizedBox(
                        height: 10.0,
                      ),
                      _crearNombre(),
                      _crearApellido1(),
                      _crearApellido2(),
                      _crearTelefono(),
                      _crearEmail(),
                      _crearPassword(),
                      SizedBox(
                        height: 10.0,
                      ),
                      _crearBoton(context),
                      FlatButton(
                        child: Text('¿Ya estás registrado?'),
                        onPressed: () {
                          Navigator.pushReplacement(
                              context, BouncyPageRoute(widget: LoginPage()));
                        },
                      )
                    ],
                  ),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget _crearNombre() {
    return TextFormField(
      initialValue: usuario.nombre,
      textCapitalization: TextCapitalization.sentences,
      decoration: InputDecoration(
          icon: Icon(Icons.person, color: Colors.blue[500]),
          labelText: 'Nombre'),
      onSaved: (value) => usuario.nombre = value,
      validator: (value) {
        if (value.isEmpty) {
          return 'espacio requerido';
          
        } else {
          return null;
        }
      },
    );
  }

  Widget _crearApellido1() {
    return TextFormField(
      initialValue: usuario.apellido1,
      textCapitalization: TextCapitalization.sentences,
      onSaved: (value) => usuario.apellido1 = value,
        validator: (value) {
        if (value.isEmpty) {
          return 'espacio requerido';
          
        } else {
          return null;
        }
      },
      decoration: InputDecoration(
          icon: Icon(Icons.person_pin, color: Colors.blue[500]),
          labelText: 'Primer Apellido'),
    );
  }

  Widget _crearApellido2() {
    return TextFormField(
      initialValue: usuario.apellido2,
      textCapitalization: TextCapitalization.sentences,
      onSaved: (value) => usuario.apellido2 = value,
      validator: (value) {
        if (value.isEmpty) {
          return 'espacio requerido';
          
        } else {
          return null;
        }
      },
      decoration: InputDecoration(
          icon: Icon(
            Icons.person_pin_circle,
            color: Colors.blue[500],
          ),
          labelText: 'Segundo Apellido'),
    );
  }

  Widget _crearTelefono() {
    return TextFormField(
      keyboardType: TextInputType.phone,
      decoration: InputDecoration(
          icon: Icon(Icons.phone, color: Colors.blue[500]),
          labelText: 'Número Telefónico'),
      onSaved: (value) => usuario.telefono = int.parse(value),
      validator: (value) {
        
        if (utils.isNumeric(value)) {
          if (value.length < 8) {
            return 'El teléfono de contener 8 caracteres!';
          }
          return null;
        } else {
          return 'Sólo números por favor!';
        }
      },
    );
  }

  Widget _crearEmail() {
    return TextFormField(
      initialValue: usuario.email,
      keyboardType: TextInputType.emailAddress,
      onSaved: (value) => usuario.email = value,
      validator: (value) {
        if (utils.isEmail(value)) {
          return null;
        } else {
          return 'Email incorrecto!';
        }
      },
      decoration: InputDecoration(
          icon: Icon(Icons.alternate_email, color: Colors.blue[500]),
          labelText: 'Correo electrónico'),
    );
  }

  Widget _crearPassword() {
    return TextFormField(
      initialValue: usuario.clave,
      obscureText: true,
      onSaved: (value) => usuario.clave = value,
        validator: (value) {
        if (value.isEmpty) {
          return 'espacio requerido';
          
        } else {
          return null;
        }
      },
      decoration: InputDecoration(
          icon: Icon(Icons.lock_outline, color: Colors.blue[500]),
          labelText: 'Contraseña'),
    );
  }

  Widget _crearBoton(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 5.0),
      width: 300,
      child: RaisedButton.icon(
        animationDuration: Duration(seconds: 2),
        icon: Icon(Icons.save),
        label: Text('Registrar'),
        onPressed: _submit,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5.0)),
        elevation: 0.0,
        color: Colors.orange[800],
        textColor: Colors.white,
      ),
    );
  }

  Widget _crearFondo(BuildContext context) {
    final size = MediaQuery.of(context).size;

    return Container(
      height: size.height * 0.6,
      width: double.infinity,
      decoration: BoxDecoration(
          gradient: LinearGradient(colors: <Color>[
        Color.fromRGBO(46, 134, 193, 1.0),
        Color.fromRGBO(46, 115, 193, 1.0)
      ])),
    );
  }

  void _submit() async {

    if (!formKey.currentState.validate()) return;

    formKey.currentState.save();

    print('Todo Ok!');
    print(usuario.nombre);
    print(usuario.apellido1);
    print(usuario.apellido2);
    print(usuario.telefono);
    print(usuario.email);
    print(usuario.clave);

    usuarioProvider.crearUsuario(usuario);
      Map resp = await usuarioProvider.crearUsuario(usuario);

     
    if(resp['ok']){
    
      utils.mostrarAlerta(context, resp['mensaje']);
      Timer(Duration(seconds: 4),(){
          Navigator.push(context, BouncyPageRoute(widget: LoginPage())); 
      });
    } else{   
       utils.mostrarAlerta(context,resp['mensaje']);
    }
    
  }
}
